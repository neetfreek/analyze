import requests
import unittest
from unittest import mock


def _get_json_from_url(url):
    # Mock HTTP get function responsible for getting JSON files from URLs
    response = requests.get(url)
    return response.json()


def _mocked_requests_get(url):
    # Mock requests.get()
    class MockResponse:
        def __init__(self, payload_file_json, status_code):
            self.data_json = payload_file_json
            self.status_code = status_code

        def json(self):
            return self.data_json

    if url == "http://foo.bar/first.json":
        return MockResponse({"key1": "value1"}, 200)
    elif url == "http://foo.bar/second.json":
        return MockResponse({"key2": "value2"}, 200)
    elif url == "http://non-existent/foo.bar/third.json":
        raise ConnectionError()

    return MockResponse(None, 404)


class AnalyzeTestCase(unittest.TestCase):
    # Missing password, username not tested as required with argparse
    @mock.patch('requests.get', side_effect=_mocked_requests_get)
    def test_fetch(self):
        # Test successful with mocked responses from _mocked_requests_get()
        payload_file_json = _get_json_from_url("http://foo.bar/first.json")
        self.assertEqual(payload_file_json, {"key1": "value1"})
        # Test exception handling
        try:
            _get_json_from_url("http://non-existent/foo.bar/third.json")
        except ConnectionError:
            pass
        except Exception:
            self.fail("Unexpected exception raised")
        else:
            self.fail(f"Expected exception {type(ConnectionError)} not raised")

        payload_file_json = _get_json_from_url(
            "http://nonexistenturl.com/cantfindme.json")
        self.assertIsNone(payload_file_json)


## TODO: Test URL validation


if __name__ == '__main__':
    unittest.main()
