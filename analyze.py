#!/usr/bin/python3

"""A command line program calculating metrics from analyzing and comparing
JSON files.
"""
import argparse
import json
import re
import statistics
from pathlib import Path

import requests
import sys
from urllib.parse import urlparse

# Global general constant variables
REGEX_PATTERN_JSON = ".json$"
# Global JSON file parsing constant variables
KEY_TIMESTAMP = "timestamp"
KEY_RESULTS = "results"
KEY_NAME = "name"
KEY_UNIT = "unit"
KEY_MEASUREMENTS = "measurements"
# Globals variables from arguments passed from user
AUTH_USERNAME = ""
AUTH_PASSWORD = ""
URLS_JSON_FILES = []
# Global variables
LOGS_FILENAMES = []
LOGS_JSON = []
LOGS_METRICS = {}
LOGS_METRICS_LIST = []
METRIC_AVERAGE = "average"
METRIC_AVERAGE_DIFF = "averageDiff"
METRIC_COUNT = "count"
METRIC_COUNT_DIFF = "countDiff"
METRIC_MAX = "maximum"
METRIC_MAX_DIFF = "maximumDiff"
METRIC_MIN = "minimum"
METRIC_MIN_DIFF = "minimumDiff"
METRIC_ROOT_KEY = "metrics"

"""Argument Setup"""


def _set_global_variables_from_arguments():
    # Set global variables from passed arguments
    parser = _get_parser()
    args_flags, args = parser.parse_known_args()
    _set_args_auth(args_flags)
    _set_args_urls(args)
    _ensure_at_least_one_url()


def _get_parser():
    # Return parser for accepting passed arguments and displaying help
    parser = argparse.ArgumentParser(description="Pass required flag "
                                                 "arguments and at least one "
                                                 "HTTP URL pointing to a JSON "
                                                 "file.")
    parser.add_argument("-p", "--password", type=str, required=True,
                        help="basic auth password for accessing JSON file/s "
                             "from passed URLs")
    parser.add_argument("-u", "--username", type=str, required=True,
                        help="basic auth username for accessing JSON file/s "
                             "from passed URLs")
    return parser


def _set_args_auth(args_flags):
    # Assign global variables' values to passed flag values if available
    global AUTH_USERNAME
    global AUTH_PASSWORD

    if args_flags.password:
        AUTH_PASSWORD = args_flags.password
    if args_flags.username:
        AUTH_USERNAME = args_flags.username


def _set_args_urls(args):
    # Assign remaining non-flag arguments to list of URLs, if valid URLs
    global URLS_JSON_FILES

    for url in args:
        if urlparse(url).scheme and urlparse(url).netloc:
            if re.search(REGEX_PATTERN_JSON, url):
                URLS_JSON_FILES.append(url)
            else:
                print(
                    f"Ignoring invalid URL: {url} - does not point to JSON "
                    f"file")
        else:
            print(f"Ignoring invalid URL: {url}")


def _ensure_at_least_one_url():
    # Terminate program if not at least one JSON
    global URLS_JSON_FILES
    if len(URLS_JSON_FILES) < 1:
        print("No valid URLs pointing to JSON files provided, aborting")
        sys.exit(1)


"""Get JSON from HTTP GET requests"""


def _get_log_filenames(urls_to_logs, logs):
    # Return log filenames:timestamps from URLs
    filenames = []
    for i in range(len(urls_to_logs)):
        # Set filename
        filename = (
            urlparse(urls_to_logs[i]).path.rsplit("/", 1)[-1].split(".", 1)[0])
        # Map filename key to timestamp value
        filenames.append({filename: logs[i][KEY_TIMESTAMP]})
    return filenames


def _get_logs_contents(urls_to_logs):
    # Return log data from URLs
    list_logs = []

    for url in urls_to_logs:
        list_logs.append(_get_json_from_url(url))
    if len(list_logs) < 2:
        print("Please pass two valid HTTPs URLs pointing to JSON files")
        sys.exit(1)
    return list_logs


def _get_json_from_url(url):
    # Return decoded JSON payload as dictionary
    try:
        headers = {"Accept": "application/json"}
        response = requests.get(url, auth=(AUTH_USERNAME, AUTH_PASSWORD),
                                headers=headers)
        return response.json()
    # Handle exceptions
    except json.decoder.JSONDecodeError as je:
        print(f"{type(je)} exception raised:\n{je}")
        sys.exit(1)
    except requests.HTTPError as he:
        print(f"{type(he)} exception raised:\n{he}")
        sys.exit(1)
    except requests.ConnectionError as ce:
        print(f"{type(ce)} exception raised:\n{ce}")
        sys.exit(1)
    # Catch both ConnectTimeout and ReadTimeout
    except requests.Timeout as te:
        print(f"{type(te)} exception raised:\n{te}")
        sys.exit(1)


"""Log analysis"""


def _sort_logs_by_timestamp(logs, logs_names):
    # Sort list containing JSON logs by "timestamp" value
    logs_names_sorted = []
    logs_sorted = sorted(logs, key=lambda key: key[KEY_TIMESTAMP],
                         reverse=False)
    for log in logs_sorted:
        timestamp_file = log[KEY_TIMESTAMP]
        # Sort key filename value timestamps based on logs_sorted sorting
        for name_key in logs_names:
            (key, value), = name_key.items()
            if value == timestamp_file and {key: timestamp_file} not in \
                    logs_names_sorted:
                logs_names_sorted.append({key: timestamp_file})
    return logs_sorted, logs_names_sorted


def _calculate_logs_metrics(logs):
    # Calculate metrics for each log file,
    global LOGS_METRICS

    for log in logs:
        metrics = {}
        # Calculate metrics for each log and save to new metrics dict
        for result in log[KEY_RESULTS]:
            key_name = str(result[KEY_NAME])
            # Create key for each set of measurements
            metrics[key_name] = {}
            # Calculate average
            metrics[key_name][METRIC_AVERAGE] = \
                _get_metric_average(result[KEY_MEASUREMENTS])
            # Calculate count
            metrics[key_name][METRIC_COUNT] = \
                _get_metric_count(result[KEY_MEASUREMENTS])
            # Calculate min
            metrics[key_name][METRIC_MIN] = \
                _get_metric_min(result[KEY_MEASUREMENTS])
            # Calculate max
            metrics[key_name][METRIC_MAX] = \
                _get_metric_max(result[KEY_MEASUREMENTS])
        # Add log files' metrics to list
        LOGS_METRICS_LIST.append({log[KEY_TIMESTAMP]: metrics})
        # Add all logs' metrics to dict
        LOGS_METRICS["metrics"] = LOGS_METRICS_LIST


def _get_shared_results(log_metrics, log_timestamps, index_1, index_2):
    # Return all results shared by the two given sets of logs
    keys_results_shared = []
    results_first = log_metrics[METRIC_ROOT_KEY][index_1][
        log_timestamps[0]]
    results_second = log_metrics[METRIC_ROOT_KEY][index_2][
        log_timestamps[1]]
    keys_results_first = list(results_first.keys())
    keys_results_second = list(results_second.keys())

    for key in keys_results_first:
        if key in keys_results_second:
            keys_results_shared.append(key)
    return keys_results_shared


"""Metrics calculation and extraction"""


def _get_metric_average(measurements):
    # Return average of result's measurements
    if type(measurements) is list:
        return round(statistics.mean(measurements), 2)
    return measurements


def _get_metric_count(measurements):
    # Return count of result's measurements
    if type(measurements) is list:
        return len(measurements)
    return 1


def _get_metric_min(measurements):
    # Return min of result's measurements
    if type(measurements) is list:
        return min(measurements)
    return measurements


def _get_metric_max(measurements):
    # Return max of result's measurements
    if type(measurements) is list:
        return max(measurements)
    return measurements


def _get_and_write_log_diffs(log_metrics):
    # Get diffs, save as files
    log_results = []
    log_timestamps = _get_logs_timestamps(log_metrics)
    for i in range(len(log_timestamps)):
        log_results.append(log_metrics[METRIC_ROOT_KEY][i][log_timestamps[i]])
    for i in range(len(log_results) - 1):
        keys_results_shared = _get_shared_results(log_metrics,
                                                  log_timestamps[i:i + 2], i,
                                                  i + 1)
        diffs = _get_metrics_diffs(log_results[i], log_results[i + 1],
                                   keys_results_shared)
        _get_filename_for_timestamp(log_metrics[METRIC_ROOT_KEY][i])
        filename_1 = _get_filename_for_timestamp(
            log_metrics[METRIC_ROOT_KEY][i])
        filename_2 = _get_filename_for_timestamp(
            log_metrics[METRIC_ROOT_KEY][i + 1])
        if diffs:
            _write_diffs_to_file(diffs, filename_1, filename_2)


def _get_logs_timestamps(log_metrics):
    # Return list of log timestamps, sorted by ascending date, serving as keys
    logs = []
    for log in log_metrics[METRIC_ROOT_KEY]:
        logs.append(list(log.keys())[0])
    return sorted(logs, reverse=False)


def _get_metric_diff(first_average, second_average):
    # Get diff for provided metric
    return round(second_average - first_average, 1)


def _get_metrics_diffs(log_first_results, log_second_results,
                       keys_results_shared):
    # Create dict containing shared results with diffs
    shared_metrics = {}

    for key_shared in keys_results_shared:
        shared_metrics[key_shared] = {}
        # Get average diff
        shared_metrics[key_shared][METRIC_AVERAGE_DIFF] = \
            _get_metric_diff(
                log_first_results[key_shared][METRIC_AVERAGE],
                log_second_results[key_shared][METRIC_AVERAGE])
        # Get count diff
        shared_metrics[key_shared][METRIC_COUNT_DIFF] = \
            _get_metric_diff(
                log_first_results[key_shared][METRIC_COUNT],
                log_second_results[key_shared][METRIC_COUNT])
        # Get min diff
        shared_metrics[key_shared][METRIC_MIN_DIFF] = \
            _get_metric_diff(
                log_first_results[key_shared][METRIC_MIN],
                log_second_results[key_shared][METRIC_MIN])
        # Get max diff
        shared_metrics[key_shared][METRIC_MAX_DIFF] = \
            _get_metric_diff(
                log_first_results[key_shared][METRIC_MAX],
                log_second_results[key_shared][METRIC_MAX])
    return _remove_no_diffs_metrics(shared_metrics)


def _get_filename_for_timestamp(metrics_as_timestamp):
    # Return filename key from timestamp value
    (key_timestamp, _), = metrics_as_timestamp.items()
    for name_key in LOGS_FILENAMES:
        (key, value), = name_key.items()
        if value == key_timestamp:
            return key


def _remove_no_diffs_metrics(shared_metrics):
    # Remove metrics without diffs to other log's measurements
    diffs = shared_metrics.copy()
    for diff in shared_metrics:
        diff_exists = False
        for metric in shared_metrics[diff]:
            if shared_metrics[diff][metric] != 0 \
                    or shared_metrics[diff][metric] != 0.0:
                diff_exists = True
        if diff_exists is False:
            diffs.pop(diff)
    return diffs


"""Metrics printing and writing"""


def _write_diffs_to_file(data, filename_1, filename_2):
    # Write diff data to file with name comprised of passed filenames
    filepath = Path(f"diff_{filename_1}_{filename_2}.json")
    with open(filepath, "w", encoding="utf-8") as file:
        json.dump(data, file, ensure_ascii=False, indent=4)
    file.close()


def _start_print_summary_metrics(metrics):
    # Collect summary metrics and print general output
    print("Summary metrics:")
    key_measurements_all = _get_key_measurements_all(metrics)
    for key in key_measurements_all:
        print(f"\n{key}:")
        averages_all = []
        count_all = 0
        min_current = 0
        min_file = {}
        max_current = 0
        max_file = {}
        unit = _get_unit_of_measurement(key)

        (k, v), = metrics.items()
        for results in v:
            (key_log, _), = results.items()
            if key in results[key_log]:
                # Add average metric to list for averaging later
                averages_all.append(results[key_log][key][METRIC_AVERAGE])
                # Update count metric
                count_all += results[key_log][key][METRIC_COUNT]
                # Determine if update min metric
                if not min_file:
                    min_file = {_get_filename_for_timestamp(results):
                                results[key_log][key][METRIC_MIN]}
                    min_current = results[key_log][key][METRIC_MIN]
                elif results[key_log][key][METRIC_MIN] < min_current:
                    min_file = {_get_filename_for_timestamp(results):
                                results[key_log][key][METRIC_MIN]}
                    min_current = results[key_log][key][METRIC_MIN]
                # Determine if update max metric
                if not max_file:
                    max_file = {_get_filename_for_timestamp(results):
                                results[key_log][key][METRIC_MAX]}
                    max_current = results[key_log][key][METRIC_MAX]
                elif results[key_log][key][METRIC_MAX] > max_current:
                    max_file = {_get_filename_for_timestamp(results):
                                results[key_log][key][METRIC_MAX]}
                    max_current = results[key_log][key][METRIC_MAX]

        print_summary_metrics(averages_all, count_all, max_file, min_file, unit)


def _get_unit_of_measurement(measurement):
    for log in LOGS_JSON:
        for i in range(len(log[KEY_RESULTS])):
            if log[KEY_RESULTS][i][KEY_NAME] == measurement:
                return log[KEY_RESULTS][i][KEY_UNIT]
    return ""


def print_summary_metrics(averages_all, count_all, max_file, min_file, unit):
    # Print summary metrics for user
    print(f"  Average: {_get_metric_average(averages_all)} {unit}")
    print(f"  Count: {count_all}")
    (key_min, value_min), = min_file.items()
    print(f"  Min: {value_min} {unit} (from \"{key_min}.json\")")
    (key_max, value_max), = max_file.items()
    print(f"  Max: {value_max} {unit} (from \"{key_max}.json\")")


def _get_key_measurements_all(metrics):
    # Return all measurement keys from all logs' metrics
    key_measurements_all = []
    for metrics in metrics[METRIC_ROOT_KEY]:
        # Get timestamp key, all logs' results value
        (key, value), = metrics.items()
        # Get all measurement keys for specific log
        keys_measurements = list(value.keys())
        for measurement in keys_measurements:
            if measurement not in key_measurements_all:
                key_measurements_all.append(measurement)
    return key_measurements_all


"""Program execution"""


def main():
    # Main program execution
    global LOGS_FILENAMES
    global LOGS_JSON

    _set_global_variables_from_arguments()
    LOGS_JSON = _get_logs_contents(URLS_JSON_FILES)
    LOGS_FILENAMES = _get_log_filenames(URLS_JSON_FILES, LOGS_JSON)
    LOGS_JSON, LOGS_FILENAMES = _sort_logs_by_timestamp(LOGS_JSON,
                                                        LOGS_FILENAMES)
    _calculate_logs_metrics(LOGS_JSON)
    _get_and_write_log_diffs(LOGS_METRICS)
    _start_print_summary_metrics(LOGS_METRICS)


main()
