# Analyze

- A command line program calculating metrics from analyzing and comparing JSON files
- JSON files are downloaded from passed URLs
- As many URLs can be passed as desired, they will all be analysed and diffed

## Overview

- Downloads JSON files over HTTP from a server with Basic auth
- Sorts JSON files by time
- Calculates the following metrics based on "measurements" fields of each item in the "results" array:
    - Average
    - Count
    - Min/Max
- Prints our calculated metrics
- Prints out and saves differences between provided files
- Prints the following "measurements'" summary metrics:
    - Average
    - Count
    - Min and which file contained it
    - Max and which file contained it

## Usage

- Change `analyze`'s access permission to include execute (.e.g. `chmod +x analyze` or `chmod 700 analyze`)
- Execute: `./analyze -u {basic auth username} -p {basic auth password} {n HTTP URLs pointing to JSON files}`
- Mandatory arguments:
  - `-p` basic auth password flag argument
  - `-u` basic auth username flag argument
  - At least one valid HTTP URL pointing to a JSON file

## Testing

- Execute: `python3 -m unittest analyze/analyze_test.py`
